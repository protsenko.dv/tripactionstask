#!/bin/bash

# Build docker image
docker build -t testrunner:local .

# Run tests
# Linux
docker run --user=`id -u` -v `pwd`:/tmp/test -it testrunner:local

# Windows
#docker run -v ${pwd}:/tmp/test -it testrunner:local
