FROM markhobson/maven-chrome:jdk-11

WORKDIR /tmp/test
ENTRYPOINT ["/bin/sh"]
CMD ["-c", "mvn clean install serenity:aggregate"]
