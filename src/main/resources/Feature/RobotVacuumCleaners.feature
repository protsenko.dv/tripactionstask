Feature: Robot Vacuums Cleaners page

  Scenario: The user is successfully opened "Robot Vacuum page" via Search Field
    Given User is on the Home page
    When User inputs to the search field "Robot vacuums"
    And User clicks on "Robot vacuums"
    Then User should be redirected on the Robot Vacuum page

  Scenario: The user is successfully opened "Robot Vacuum page" via Category menu
    Given User opens the Home page
    When User move mouse on category "Household & living"
    And User clicks on the link "Robot vacuums"
    Then User should be redirected on the Robot Vacuum page

  Scenario: The user successfully adds one product to Shopping Cart and able to remove it
    Given User opens the Robot Vacuum page
    When the user selects the № 1 product in the list
    And the user clicks on the button "m ready to order"
    Then the user should be redirected to Shopping Cart page
    And the selected products are displayed on Shopping Cart page
    When User clicks on the link "Remove"
    Then the Shopping Cart is empty

  Scenario: The user successfully adds several products to Shopping Cart
    Given User opens the Robot Vacuum page
    When the user selects the № 1 product in the list
    And the user clicks on the button "Continue shopping"
    And the user selects the № 2 product in the list
    And the user clicks on the button "Continue shopping"
    And the user selects the № 3 product in the list
    And the user clicks on the button "m ready to order"
    Then the user should be redirected to Shopping Cart page
    And the selected products are displayed on Shopping Cart page

  Scenario: The Product List is successfully filtered by 1 User's filter
    Given User opens the Robot Vacuum page
    When the amount of products is remembered
    And the user selects the filter with name "Blaupunkt"
    Then the proper amount of filtered products is displayed

  Scenario: The Product List is successfully filtered by 2 User's filters
    Given User opens the Robot Vacuum page
    When the amount of products is remembered
    And the user selects the filter with name "iRobot"
    Then the proper amount of filtered products is displayed
    When the user selects the filter with name "Very good"
    Then the proper amount of filtered products is displayed

  Scenario: The user is redirected to "Home" page after press the logo
    Given User opens the Robot Vacuum page
    When the user clicks on Coolblue logo
    Then the user should be redirected to "Home" page
