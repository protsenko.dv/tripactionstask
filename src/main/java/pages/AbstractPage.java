package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static managers.ConfigFileManager.getDefaultTimeout;
import static utils.CustomAsserts.invisibilityOfAllElementsLocatedBy;

public abstract class AbstractPage {
    private static final Logger logger = LogManager.getLogger(AbstractPage.class);
    private final WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    protected FluentWait<WebDriver> newWait() {
        return new WebDriverWait(driver, Integer.parseInt(getDefaultTimeout()))
                .ignoring(StaleElementReferenceException.class);
    }

    public void switchToLastOpenedTab() {
        Set<String> handleList = driver.getWindowHandles();
        ArrayList<String> listOfTabs = new ArrayList<>(handleList);
        driver.switchTo().window(listOfTabs.get(listOfTabs.size() - 1));
        logger.info("Switched to new tab");
    }

    public String getCurrentUrl() {
        String url = driver.getCurrentUrl();
        logger.info(url);
        return url;
    }

    protected WebElement findElement(By by) {
        return driver.findElement(by);
    }

    protected List<WebElement> waitForElements(By by) {
        return newWait().until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    protected WebElement waitForElementToBeClickable(By by) {
        return newWait().until(ExpectedConditions.elementToBeClickable(by));
    }

    protected void clickButtonWithText(String text) {
        logger.info("click a button with text: " + text);
        waitForElementToBeClickable(By.xpath("//*[contains(text(),'" + text + "')]")).click();
    }

    protected WebElement waitUntilElementAppears(By by) {
        return newWait().until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public boolean waitUntilElementDisappears(By by) {
        return newWait().until(invisibilityOfAllElementsLocatedBy(by));
    }

    protected void moveToAnElement(By by) {
        new Actions(driver).moveToElement(waitUntilElementAppears(by)).perform();
    }

    protected void sendTextToFieldBy(By by, String inputText) {
        WebElement element = waitForElementToBeClickable(by);
        element.clear();
        element.click();
        element.sendKeys(inputText);
        logger.info("\"" + inputText + "\" text was input to the field");
    }

    public void clickOnElement(By by) {
        waitForElementToBeClickable(by).click();
    }

    public abstract void pageIsLoaded();

    public abstract void verifyUrl();

    public void acceptCookies() {
        clickOnElement(By.name("accept_cookie"));
        logger.info("Cookies are accepted!");
    }

    public void clickOnLink(String link){
        waitForElementToBeClickable(By.linkText(link)).click();
    }
}
