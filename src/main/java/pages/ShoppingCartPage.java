package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;
import java.util.stream.Collectors;

import static managers.ConfigFileManager.*;
import static org.junit.Assert.assertTrue;
import static utils.CustomAsserts.assertEventually;

public class ShoppingCartPage extends AbstractPage {

    private static final Logger logger = LogManager.getLogger(ShoppingCartPage.class);
    private final WebDriver driver;

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public void pageIsLoaded() {
        assertEventually(5, 1000, () -> assertTrue(findElement(
                By.xpath("//title[contains(text(),'Shopping cart')]")).isEnabled()));
    }

    @Override
    public void verifyUrl() {
        assertEventually(3, 500, () -> assertTrue("Shopping Cart page is not open", getCurrentUrl().endsWith(
                getPropertyValueByName(SHOPPING_CART_URL)
        )));

    }

    public void open() {
        driver.get(getDefaultUrl() + getPropertyValueByName(SHOPPING_CART_URL));
        logger.info("Opening Shopping Cart Page !!!!");
    }

    public Set<String> getProductsOnTheShoppingCartPage() {
        return waitForElements(By.xpath("//div[@data-shopping-cart-item-product-id]"))
                .stream().map(e -> e.getAttribute("data-shopping-cart-item-product-id"))
                .collect(Collectors.toSet());
    }

    public boolean theShoppingCartIsEmpty() {
        return waitUntilElementDisappears(By.xpath("//div[@data-shopping-cart-item-product-id]"));
    }

}
