package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import static managers.ConfigFileManager.*;
import static org.junit.Assert.assertTrue;
import static utils.CustomAsserts.assertEventually;

public class RobotVacuumsPage extends AbstractPage {

    private static final Logger logger = LogManager.getLogger(RobotVacuumsPage.class);
    private final WebDriver driver;

    public RobotVacuumsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public void clickButtonText(String buttonText) {
        clickButtonWithText(buttonText);
    }

    @Override
    public void pageIsLoaded() {
        assertEventually(5, 1000, () -> assertTrue(findElement(
                By.xpath("//title[contains(text(),'Buy Robot vacuum? - Coolblue')]")).isEnabled()));
    }

    public void open() {
        driver.get(getDefaultUrl() + getPropertyValueByName(ROBOT_VACUUMS_URL));
        logger.info("Opening Robot Vacuums Page !!!!");
    }

    @Override
    public void verifyUrl() {
        assertEventually(3, 500, () -> assertTrue("Robot vacuums is not open",
                getCurrentUrl().endsWith(getPropertyValueByName(ROBOT_VACUUMS_URL))));
    }

    public void clickLogo() {
        waitForElementToBeClickable(By.xpath(".//a[@title='Coolblue home']")).click();
    }

    public void clickPrivacy() {
        waitForElementToBeClickable(By.xpath(".//a[@data-autotest-id='mr-link-privacy-1']")).click();
    }

    public String selectProductFromTheList(int index) {
        WebElement webElement = waitForElements(By.xpath("//div[@data-atc-product-data]")).get(index);
        webElement.findElement(By.tagName("button")).click();
        int x = new JSONObject(webElement.getAttribute("data-atc-product-data"))
                .getJSONArray("productIds")
                .getInt(0);
        logger.info("Product ID is: " + x);
        return String.valueOf(x);
    }

    public Integer getTotalAmountOfProducts() {
        return Integer.valueOf(waitUntilElementAppears(
                By.xpath("//strong[@class='filtered-search__result-count-number']")).getText());
    }

    public Integer checkFilterAndGetItsAmount(String filterName) {
        String filterXpath = "//span[contains(text(),'" + filterName + "')]";
        String amountXpath = "/parent::span//span[@class='additional-text']";
        Integer amount = Integer.valueOf(waitUntilElementAppears(
                By.xpath(filterXpath + amountXpath)).getText().trim().replaceAll("[()]",""));
        waitForElementToBeClickable(By.xpath(filterXpath)).click();
        return amount;
    }
}
