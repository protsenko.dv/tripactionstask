package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static managers.ConfigFileManager.getDefaultUrl;
import static org.junit.Assert.assertTrue;
import static utils.CustomAsserts.assertEventually;

public class HomePage extends AbstractPage {
    private static final Logger logger = LogManager.getLogger(HomePage.class);
    private final WebDriver driver;

    public HomePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public void pageIsLoaded() {
        assertEventually(5, 500, () -> assertTrue(
                findElement(By.xpath("//title[contains(text(),'Coolblue - anything for a smile')]")).isEnabled()));
    }

    @Override
    public void verifyUrl() {
        assertEventually(3, 500, () -> assertTrue("Home Page is not open",
                getCurrentUrl().contains("coolblue.nl")));
    }


    public void inputsToSearchField(String text) {
        sendTextToFieldBy(By.id("search_query"), text);
    }

    public void clickOn(String item) {
        clickButtonWithText(item);
    }

    public void moveMouseOnCategory(String category) {
        moveToAnElement(By.xpath("//li[@data-category-group='" + category + "']"));
    }

    public void open() {
        driver.get(getDefaultUrl());
        logger.info("Opening Home Page !!!!");
    }
}
