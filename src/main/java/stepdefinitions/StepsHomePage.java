package stepdefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;

import static runners.TestRunner.pageManager;

public class StepsHomePage {

    private final HomePage homePage = pageManager.getHomePage();

    @Given("^User is on the Home page$")
    @Then("^the user should be redirected to \"Home\" page$")
    public void redirectHomePage() {
        homePage.verifyUrl();
        homePage.pageIsLoaded();
    }

    @Given("^User opens the Home page$")
    public void openHomePage() {
        homePage.open();
        redirectHomePage();
    }

    @When("^User inputs to the search field \"(.*)\"$")
    public void userInputsToTheSearchField(String text) {
        homePage.inputsToSearchField(text);
    }

    @When("^User clicks on \"(.*)\"$")
    public void userClickOn(String menuItem) {
        homePage.clickOn(menuItem);
    }

    @When("^User move mouse on category \"(.*)\"$")
    public void userMoveMouseOn(String category) {
        homePage.moveMouseOnCategory(category);
    }

    @And("^User clicks on the link \"(.*)\"$")
    public void userClicksOnTheLink(String link) {
        homePage.clickOnLink(link);
    }
}
