package stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.RobotVacuumsPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static runners.TestRunner.pageManager;
import static stepdefinitions.StepCommons.getScenario;
import static utils.CustomAsserts.assertEventually;
import static utils.ValuesStorage.StoredValues.FILTERED_AMOUNT;
import static utils.ValuesStorage.StoredValues.TOTAL_AMOUNT;
import static utils.ValuesStorage.*;

public class StepsRobotVacuumsPage {
    private final RobotVacuumsPage robotVacuumsPage = pageManager.getSignUpPage();

    @Given("^User opens the Robot Vacuum page$")
    public void openVacuumPage() {
        robotVacuumsPage.open();
        onRobotVacuumPage();
    }

    @Given("^User is on the Robot Vacuum page$")
    @Then("^User should be redirected on the Robot Vacuum page$")
    public void onRobotVacuumPage() {
        robotVacuumsPage.verifyUrl();
        robotVacuumsPage.pageIsLoaded();
    }

    @Given("^User clicks on the button \"(.*)\"$")
    @When("^the user clicks on the button \"(.*)\"$")
    public void clickOnButton(String buttonText) {
        robotVacuumsPage.clickButtonText(buttonText);
    }

    @When("^the user clicks on Coolblue logo$")
    public void clickOnLogo() {
        robotVacuumsPage.clickLogo();
    }

    @When("^the user clicks on \"Privacy Policy\" link$")
    public void clickOnPrivacyLink() {
        robotVacuumsPage.clickPrivacy();
        robotVacuumsPage.switchToLastOpenedTab();
    }

    @When("^the user selects the № (.*) product in the list$")
    public void theUserSelectsFirstProduct(int index) {
        saveProductId(getScenario().getName(),
        robotVacuumsPage.selectProductFromTheList(index - 1));
    }

    @When("^the user selects the filter with name \"(.*)\"$")
    public void selectsFilterWithName(String filterName) {
        Integer amount =robotVacuumsPage.checkFilterAndGetItsAmount(filterName);
        saveAmount(FILTERED_AMOUNT,getScenario().getName(),amount);
    }

    @When("^the amount of products is remembered$")
    public void amountOfProductsIsRemembered() {
        Integer amount = robotVacuumsPage.getTotalAmountOfProducts();
        saveAmount(TOTAL_AMOUNT,getScenario().getName(),amount);
    }

    @Then("^the proper amount of filtered products is displayed$")
    public void theProperAmountOfProductsIsDisplayed() {
        assertEventually(3, 500, () -> {
            Integer currentAmount = robotVacuumsPage.getTotalAmountOfProducts();
            Integer filteredAmount = getAmount(FILTERED_AMOUNT, getScenario().getName());
            assertTrue(getAmount(TOTAL_AMOUNT, getScenario().getName()) > currentAmount);
            assertEquals(filteredAmount, currentAmount);
        });
    }
}
