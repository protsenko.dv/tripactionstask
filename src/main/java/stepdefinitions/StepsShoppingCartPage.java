package stepdefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.ShoppingCartPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static runners.TestRunner.pageManager;
import static stepdefinitions.StepCommons.getScenario;
import static utils.ValuesStorage.getProductIds;

public class StepsShoppingCartPage {

    private final ShoppingCartPage shoppingCartPage = pageManager.getShoppingCartPage();

    @Then("^the user should be redirected to Shopping Cart page$")
    public void redirectShoppingCartPage() {
        shoppingCartPage.verifyUrl();
        shoppingCartPage.pageIsLoaded();
    }

    @When("^the user opens Shopping Cart page$")
    public void userOpensPage() {
        shoppingCartPage.open();
        redirectShoppingCartPage();
    }

    @Then("^the selected products are displayed on Shopping Cart page$")
    public void productAreDisplayed() {
        assertEquals(getProductIds(getScenario().getName()),
                shoppingCartPage.getProductsOnTheShoppingCartPage());
    }

    @Then("^the Shopping Cart is empty$")
    public void productsAreNotDisplayed() {
        assertTrue("Removed product is displayed ",
                shoppingCartPage.theShoppingCartIsEmpty());
    }
}
