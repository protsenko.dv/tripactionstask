package stepdefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class StepCommons {
    private static final Logger log = LogManager.getLogger(StepCommons.class);
    private static ThreadLocal<Scenario> scenario = new ThreadLocal<>();

    @Before
    public void beforeScenario(Scenario scen) {
        scenario.set(scen);
        log.info("*******  Before Scenario ********");
        log.info("Executing Scenario - " + scen.getName());
    }

    @After
    public void afterScenario(Scenario scen) {
        log.info("****** Executing After Scenario *******");
        log.info("Scenario " + scen.getName() + " Complete with Status " + scen.getStatus());
        scenario.remove();
    }

    public static Scenario getScenario(){
        return scenario.get();
    }


}
