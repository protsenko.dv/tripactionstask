package managers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileManager {

    public static final String PROPERTIES_PATH = "data.properties";
    public static final String DEFAULT_URL = "base.url";
    public static final String ROBOT_VACUUMS_URL = "robot.vacuums.url";
    public static final String SHOPPING_CART_URL = "shopping.cart.url";
    private static final String DEFAULT_BROWSER = "browser";
    private static final String DEFAULT_TIMEOUT = "explicit.timeout";
    private static final String IS_BROWSER_SHOWN = "is.browser.shown";

    private static final Logger log = LogManager.getLogger(ConfigFileManager.class);


    public static Properties loadProperties() {
        Properties properties = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(PROPERTIES_PATH))) {
            properties = new Properties();
            properties.load(reader);
        } catch (IOException e) {
            log.error("Not able to Load property file !!");
            e.printStackTrace();
        }
        return properties;
    }

    public static String getPropertyValueByName(String name) {
        return loadProperties().getProperty(name);
    }

    public static String getDefaultUrl() {
        return getParams("defaultUrl", getPropertyValueByName(DEFAULT_URL));
    }

    public static String getDefaultBrowser() {
        return getParams("browser", getPropertyValueByName(DEFAULT_BROWSER));
    }

    public static String getDefaultTimeout() {
        return getParams("explicitTimeout", getPropertyValueByName(DEFAULT_TIMEOUT));
    }

    public static boolean isBrowserShown() {
        String isBrowserShown = getParams("isBrowserShown", getPropertyValueByName(IS_BROWSER_SHOWN));
        return isBrowserShown.contains("true");
    }

    private static String getParams(String variableName, String defaultValue) {
        String env = System.getenv(variableName);
        if (env != null) {
            variableName = defaultValue;
        }
        String result = System.getProperty(variableName, defaultValue);
        if (result == null) {
            result = defaultValue;
        }
        return result;
    }

}

