package managers;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageManager {
    private final WebDriver driver;
    private RobotVacuumsPage robotVacuumsPage;
    private HomePage homePage;
    private ShoppingCartPage shoppingCartPage;

    public PageManager(WebDriver driver) {
        this.driver = driver;
    }

    public RobotVacuumsPage getSignUpPage() {
        return (robotVacuumsPage == null) ? robotVacuumsPage = new RobotVacuumsPage(driver) : robotVacuumsPage;
    }

    public ShoppingCartPage getShoppingCartPage() {
        return (shoppingCartPage == null) ? shoppingCartPage = new ShoppingCartPage(driver) : shoppingCartPage;
    }

    public HomePage getHomePage() {
        return (homePage == null) ? homePage = new HomePage(driver) : homePage;
    }

}
