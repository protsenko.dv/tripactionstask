package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CustomAsserts {
    private static final Logger logger = LogManager.getLogger(CustomAsserts.class);

    public static void assertEventually(long timeout, long pause, Runnable junitAssertion) {
        final long end = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);
        while (System.currentTimeMillis() <= end) {
            if (tryAssert(pause, junitAssertion)) return;
        }
        junitAssertion.run();
    }

    public static void assertEventually(int attempts, long pause, Runnable junitAssertion) {
        for (; attempts > 0; attempts--) {
            if (tryAssert(pause, junitAssertion)) return;
        }
        junitAssertion.run();
    }

    private static boolean tryAssert(long pause, Runnable junitAssertion) {
        try {
            junitAssertion.run();
            return true;
        } catch (Exception | AssertionError e) {
            logger.debug("Assertion attempt failed: {}", e.getMessage());
        }
        try {
            Thread.sleep(pause);
        } catch (InterruptedException re) {
            throw new RuntimeException(re);
        }
        return false;
    }

    /**
     * An expectation for checking that all elements present on the web page that match the locator are invisible. Visibility means
     * that the elements are not only displayed but also have a height and width that is greater than 0.
     *
     * @param locator used to find the element
     * @return true if all elements are invisible, false otherwise
     */
    public static ExpectedCondition<Boolean> invisibilityOfAllElementsLocatedBy(final By locator) {
        return new ExpectedCondition<Boolean>() {

            @Override
            public Boolean apply(WebDriver driver) {
                List<WebElement> elements = driver.findElements(locator);
                try {
                    for (WebElement element : elements) {
                        if (element.isDisplayed()) {
                            return false;
                        }
                    }
                    return true;
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "invisibility of all elements located by " + locator;
            }

        };
    }

}
