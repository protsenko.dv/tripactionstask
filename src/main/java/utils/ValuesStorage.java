package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static utils.ValuesStorage.StoredValues.*;

public class ValuesStorage {
    private static final Logger logger = LogManager.getLogger(ValuesStorage.class);
    public static ThreadLocal<Map<StoredValues, Object>> valueStorage = new ThreadLocal<>();

    synchronized public static void setValueStorage() {
        getValueStorage().put(PRODUCT_IDS, new HashMap<String, Set<String>>());
        getValueStorage().put(TOTAL_AMOUNT, new HashMap<String, Integer>());
        getValueStorage().put(FILTERED_AMOUNT, new HashMap<String, Integer>());
    }

    synchronized private static Map<StoredValues, Object> getValueStorage() {
        if (valueStorage.get() == null) {
            valueStorage.set(new HashMap<>());
        }
        return valueStorage.get();
    }

    synchronized public static void saveProductId(String scenario, String id) {
        Set<String> ids = getProductIds(scenario);
        if(ids == null){
            logger.info("The Product ID for scenario: " + scenario + " has not found!! \n");
            ids = new HashSet<>();
        }
        ids.add(id);
        Map<String, Set<String>> idsOfScenarios = getAllProductIds();
        idsOfScenarios.put(scenario, ids);
        getValueStorage().put(PRODUCT_IDS, idsOfScenarios);
        logger.info("Saved Product ID to storage: " + id + "\n");
    }

    synchronized public static Set<String> getProductIds(String scenario) {
        Map<String, Set<String>> idsOfScenarios = getAllProductIds();
        return idsOfScenarios.get(scenario);
    }

    synchronized private static Map<String, Set<String>> getAllProductIds() {
        return (Map<String, Set<String>>) getValueStorage().get(PRODUCT_IDS);
    }


    synchronized public static void saveAmount(StoredValues storedValues, String scenario, Integer amount) {
        Map<String, Integer> amountOfAllScenarios = (Map<String, Integer>) getValueStorage().get(storedValues);
        amountOfAllScenarios.put(scenario, amount);
        getValueStorage().put(storedValues, amountOfAllScenarios);
        logger.info("Saved amount to storage: " + amount + "\n");
    }

    synchronized public static Integer getAmount(StoredValues storedValues, String scenario) {
        Map<String, Integer> amountOfAllScenarios = (Map<String, Integer>) getValueStorage().get(storedValues);
        return amountOfAllScenarios.get(scenario);
    }

    public enum StoredValues {
        PRODUCT_IDS,
        TOTAL_AMOUNT,
        FILTERED_AMOUNT
    }
}
