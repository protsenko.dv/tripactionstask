package runners;

import cucumber.api.CucumberOptions;
import initializedriver.DriverProvider;
import managers.PageManager;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import static managers.ConfigFileManager.getDefaultUrl;
import static utils.ValuesStorage.setValueStorage;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = {
                "src/main/resources/Feature/RobotVacuumCleaners.feature"
        }
        , glue = "stepdefinitions"
)

public class TestRunner {

    private static final Logger logger = LoggerFactory.getLogger(TestRunner.class);
    private static final DriverProvider driverProvider;
    public static PageManager pageManager;

    static {
        logger.info("Setting up WebDriver !!");
        driverProvider = new DriverProvider();
        logger.info("Initializing Value Storage !!");
        setValueStorage();
        logger.info("Initializing Page Manager !!");
        pageManager = new PageManager(driverProvider.getWebDriver());
        logger.info("Opening URL" + getDefaultUrl());
        driverProvider.getWebDriver().get(getDefaultUrl());
        pageManager.getHomePage().acceptCookies();
    }

    @BeforeClass
    public static void setup() {
        try (PrintStream printStream = new PrintStream(new FileOutputStream(System.getProperty("user.dir")
                + "/logs/test.log", false), false)) {
            System.setOut(printStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @AfterClass
    public static void tearDown() {
        driverProvider.driverQuit();
    }

    public DriverProvider getDriverProvider() {
        return driverProvider;
    }

    public PageManager getPageManager() {
        return pageManager;
    }


}
