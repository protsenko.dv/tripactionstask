package initializedriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import static managers.ConfigFileManager.getDefaultBrowser;
import static managers.ConfigFileManager.isBrowserShown;

public class DriverProvider {
    private static final Logger logger = LogManager.getLogger(DriverProvider.class);
    private WebDriver driver;

    public WebDriver getWebDriver() {
        if (driver == null) {
            driver = createWebDriver();
        }
        return driver;
    }

    public void driverQuit() {
        if (driver != null) {
            driver.quit();
            logger.info("Closed all Webdriver Instances !!!");
        }
    }

    private WebDriver createWebDriver() {
        switch (getDefaultBrowser()) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                if (!isBrowserShown()) {
                    chromeOptions.addArguments("--headless");
                }
                chromeOptions.addArguments(
                        "--disable-gpu",
                        "--start-maximized",
                        "--ignore-certificate-errors",
                        "--disable-extensions",
                        "--no-sandbox",
                        "--disable-dev-shm-usage");
                driver = new ChromeDriver(chromeOptions);
                logger.info("Starting Chrome !!!");
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxBinary firefoxBinary = new FirefoxBinary();
                FirefoxOptions ffOptions = new FirefoxOptions();
                ffOptions.setBinary(firefoxBinary);
                ffOptions.setHeadless(!isBrowserShown());
                driver = new FirefoxDriver(ffOptions);
                logger.info("Starting Firefox !!!");
                break;
        }
        return driver;
    }


}
