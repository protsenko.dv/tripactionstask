# TripActions Assessment Task #

Test Automation framework for TripActions's QA Test Automation assignment created specifically for company recruitment process.
Some tests for https://www.coolblue.nl/en have been automated. However, the project provides an example of the architecture of a test automation framework for a frontend in a BDD framework implemented using Cucumber, Serenity and Web Driver.

## Overview ##
This test framework includes WebDriver tests for TripActions signup form. The framework is based on BDD achieved using Web driver library to handle the frontend tests:<br>
https://www.coolblue.nl/en

## Test Scenarios ##
7 test Scenarios are here: src/main/resources/Feature in the file RobotVacuumCleaners.feature
https://gitlab.com/protsenko.dv/tripactionstask/-/blob/master/src/main/resources/Feature/RobotVacuumCleaners.feature

## How To Run ##
        In case you have acess to https://gitlab.com/protsenko.dv/tripactionstask/-/pipelines
        1. go to CI/CD press "Run pipeline" button

        In case you have Linux and installed Docker on your local machine:
        1. clone the project
        2. go to the project directory
        3. execute shell script -  run.sh
                ./run.sh
                
        In case you have Windows and installed Docker:
        1. go to the project directory
        2. Find the script run.sh, open it
        3. Please comment (by #) the row: docker run --user=`id -u` -v `pwd`:/tmp/test -it testrunner:local
        4. Please uncomment row: docker run -v ${pwd}:/tmp/test -it testrunner:local
        5. save the file
        6. run script ./run.sh
        
        In case you have Linux and installed java 8 (and higher) and maven 3:
        1. go to the project directory
        2. run command: 
            mvn clean install serenity:aggregate

## Browsers ##
For correct work of the tests you need specify the browser in the config file data.properties:
browser=chrome OR browser=firefox.
Also, you can specify browser in environment variables:
-Dbrowser=chrome OR -Dbrowser=firefox

Also, here you can specify how do you want to run browser, in headless mode or in the normal one.
is.browser.shown=false (true) - in the data.properties
-DisDrowserShown=false (true) - in the environment variables

NOTE: By default set the Docker image with Chrome. If you want to run container with FireFox you have to change docker image in Dockerfile
markhobson/maven-chrome:jdk-11 to markhobson/maven-firefox


## Configuration file ##
Please find the configuration file in the project directory: data.properties

## Reporting ##
The reporting tool is serenity.
After the running of tests please find the report:

        In case you are on https://gitlab.com/protsenko.dv/tripactionstask/-/pipelines
        1. Open last build
        2. Press to "deploy" button
        3. Browse the job artifact
        4. Open the target/site/serenity/index.html
        5. accept the unsecure connection

        In case you run tests on your local machine:
        1. open target/site/serenity/index.html

## Logs ##
After the running of tests, please find the logs in the project directory: *logs*


# Tools Used
Programming Language - Java<br/>
Web Drivers: ChromeDriver, FireFoxDriver
Framework Details - BDD using Cucumber and Gherkin Syntax<br/>
Reporting - Serenity<br/>

# Had to do
Design Specification: Created Robot Vacuum cleaner search category.
Created filters to filter out results based on the different criteria. Add to cart button.
url: https://www.coolblue.nl/en
Main page -> ‘Household & living’ -> ‘Robot vacuums’
Technical task:
● Setup automation framework (use any tools and frameworks that you find suits best
this task)
● Come up with at least 5 test cases, describe them in README.md
● Automate end-to-end flow until checkout for the above spec
● Include README.md instruction how to run, which tools used with brief description
why you chose them